#include <stdio.h>
// Add your system includes here.
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include "ftree_visualizer.h"
#include "hash.h"

// works with directory less than 1000 files
#define MAXDIRS 1000

/*
 * Returns the FTree rooted at the path fname.
 */
struct TreeNode *generate_ftree(const char *fname) {
    // Your implementation here.
    struct TreeNode *result = malloc(sizeof(struct TreeNode));
    struct stat path_;

    if (lstat(fname, &path_) != 0){
    		perror("lstat");
    		exit(1);
    	}

    else  {
      if (!(S_ISDIR(path_.st_mode))) {
          FILE *single_file;
          single_file = fopen(fname, "r");
          if (single_file == NULL) {
            perror("Error opening file\n");
          }
          // extract filename out of directory path
          char *filename;
          filename = strrchr(fname, '/');

          // don't have to check for '.' if file isn't NULL cuz directory
          // recusion already check for that
          if ((filename != NULL) || ((filename == NULL) && (fname[0] != '.'))) {
          // if statement for if argument is just a single file
          if (filename != NULL) {
            int size = strlen(filename + 1)+1;
            result->fname = malloc(size);
            strncpy(result->fname, (filename + 1), size);
            result->fname[size] = '\0';
          }

          else {
            result->fname = malloc(strlen(fname));
            strncpy(result->fname, fname, strlen(fname));
            result->fname[strlen(fname)] = '\0';
          }
          result->contents = NULL;
          result->hash = hash(single_file);
          result->permissions = (path_.st_mode & 0777);
          }
      return result;
      }

      else {
        struct dirent dirs[MAXDIRS];
        struct dirent *dp;
        DIR *dirp = opendir(fname);
        if(dirp == NULL) {
          perror("Directory cannot be opening\n");
        }

        // extract directory name if it's directory in directory
        char *path_name;
        path_name = strrchr(fname, '/');

        if (path_name != NULL) {
          result->fname = malloc(strlen(path_name + 1));
          strncpy(result->fname, path_name + 1, strlen(path_name + 1));
        }

        else if (path_name == NULL) {
          result->fname = malloc(strlen(fname));
          strncpy(result->fname, fname, strlen(fname));
        }

        result->hash = NULL;
        result->permissions = (path_.st_mode & 0777);

        int i;
        i = 0;
        while (i < MAXDIRS && (dp = readdir(dirp)) != NULL) {
          if ((*dp).d_name[0] != '.') {
            dirs[i] = *dp;
            i++;
          }
        }

        if (i != 0) {
          char *new_path;
          int size = strlen(fname) + 1;
          new_path = malloc(size+1);
          strcpy(new_path, fname);
          strncat(new_path, "/", size);
          new_path[size+1] = '\0';

          char *use_path;
          int j;
          use_path = malloc(size + strlen(dirs[0].d_name) + 1);
          strncat(use_path, new_path, (size + strlen(dirs[0].d_name) + 1));
          strncat(use_path, dirs[0].d_name, (size + strlen(dirs[0].d_name) + 1));

          struct TreeNode *populate = generate_ftree(use_path);
          result->contents = populate;

          for (j = 1; j < i; j++) {
            int clear_size = size + strlen(dirs[j].d_name) + 1;
            use_path = malloc(clear_size);
            strncat(use_path, new_path, clear_size);
            strncat(use_path, dirs[j].d_name, clear_size);
            populate->next = generate_ftree(use_path);
            populate = populate->next;
          }
        }
        return result;
      }
    }
}


/*
 * Prints the TreeNodes encountered on a preorder traversal of an FTree.
 */
void print_ftree(struct TreeNode *root) {
    // Here's a trick for remembering what depth (in the tree) you're at
    // and printing 2 * that many spaces at the beginning of the line.
    if (root->fname == NULL) {
      fprintf(stderr, "TreeNode is NULL\n");
    }

    else {
      static int depth = 0;
      printf("%*s", depth * 2, "");

      if (root->hash != NULL) {
        printf("%s (%d) \n",root->fname, root->permissions);
      }

      else {
        printf("===== %s (%d) =====\n", root->fname, root->permissions);

        if ((root->contents) != NULL) {
          depth++;
          print_ftree(root->contents);
          root = root->contents;
          // for empty folder
          while((root->next) != NULL) {
            print_ftree(root->next);
            root = root->next;
          }
          depth--;
        }
      }
  }
}
