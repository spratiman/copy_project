README

### Program Requirements ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Features ###

### How to use print_ftree ###

* Compile: gcc -Wall -std=gnu99 -g -o print_ftree print_ftree.c hash_functions.c ftree_visualizer.c 
* Run: print_ftree "path"
* This will give you the tree representation of the directory at "path" with hash values next to the directory/file names