#include <stdio.h>
#include <stdlib.h>
#include "hash.h"
#define BLOCK_SIZE 8

char *hash(FILE *f) {
  char *hash_file = malloc((sizeof(char) * 8) + 1);
  for(int i = 0; i < BLOCK_SIZE + 1; i++) {
    hash_file[i] = '\0';
  }
  char file_array[sizeof(f)]; /* variable to store input arguments */

  int i = 0;
  while(fread(file_array, sizeof(char), sizeof(f), f) == sizeof(f)) {
    hash_file[i] = file_array[i] ^ hash_file[i];

    if ((i < BLOCK_SIZE - 1) && ( 0 <= i)) {  /* block_size is smaller than
                                                          input char string */
      i++;
    } else {
      i = 0;
    }
  }

  return hash_file;

}

int check_hash(const char *hash1, const char *hash2, long block_size) {

  for (int i = 0; i < block_size; i++){
    if(hash1[i] != hash2[i]) {  /* first index value that doesn't match for both
                                                              hash inputs */
      return i;
    }
  }
    return block_size;
}
