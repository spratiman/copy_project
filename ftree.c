//Header files
#include "ftree.h"
#include "hash.h"
//System Includes
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <libgen.h>
#include <sys/wait.h>
//This program would compile and run for directory with less than 1000 files
#define MAXDIRS 1000


/* Function for computing the size of the file
 * Returns the file size.
 */
int get_file_size(FILE *file) {

  int file_size = 0;

  fseek(file, 0, SEEK_END);  //Seeking to the end of the file
  file_size = ftell(file);   //Assigning the position of the end
  fseek(file, 0, SEEK_SET);  //Seeking back to the beginning

  return file_size;
 }

/* Function to copy the regular file from source path into a
 * new file, to destination path
 * Returns -1 if an error is encountered in this process or
 * it returns 0 if the copying is successful.
 */
 int copy_file(const char *src, const char *dest) {

   FILE *src_file, *dest_file;
   int src_file_size = 0;
   int dest_file_size = 0;

   src_file = fopen(src, "rb");
   if (src_file == NULL) {
     perror("Error opening file\n");
     return -1;
   }
   src_file_size = get_file_size(src_file);

   int copy_needed = 0;
   dest_file = fopen(dest, "rb");
   if (dest_file) {
     dest_file_size = get_file_size(dest_file);
   }

  if (src_file_size == dest_file_size && src_file_size != 0 && src_file && dest_file) {
    char *src_hash = NULL;
    char *dest_hash = NULL;
    src_hash = hash(src_file);
    dest_hash = hash(dest_file);
    if (strcmp(src_hash, dest_hash) != 0) { //size matches and hash doesnt, copy needed
      copy_needed = 1;
    }
    free(src_hash);
    free(dest_hash);
  } else { //size doesn't match, copy needed
    copy_needed = 1;
  }

  if (copy_needed) {
    if (dest_file) {
      fclose(dest_file);
    }
    dest_file = fopen(dest, "wb");
    int buffer_size = 1024;
    char *buffer = malloc(buffer_size);

    while (src_file_size > 0) {
      int bytes_to_read = buffer_size;
      if (src_file_size < buffer_size) {
          bytes_to_read = src_file_size;
        }
      fread(buffer, bytes_to_read, 1, src_file);
      fwrite(buffer, bytes_to_read, 1, dest_file);
      src_file_size -= bytes_to_read;
      }
      free(buffer);
    }
   if (dest_file) {
     fclose(dest_file);
   }
   fclose(src_file);

   return 0;
 }

/* Function for copying a file tree rooted at src to dest
 * Returns < 0 on error. The magnitude of the return value
 * is the number of processes involved in the copy and is
 * at least 1.
 */
int copy_ftree(const char *src, const char *dest) {

  int num_of_processes = 1;
  struct stat status;
  if (lstat(src, &status) == -1) {  //Deal with unsuccessful lstat
    perror("lstat");
    return -num_of_processes;
  }
  if (S_ISDIR(status.st_mode)) {  //source is a directory
    struct dirent *direntry;
    DIR *dir = opendir(src);
    if (dir == NULL) {
      perror("opendir");
      return -num_of_processes;
    } else {  //THere are no errors while opening the dir
        int pid_array[1000];
        int num_of_pid = 0;
        while ((direntry = readdir(dir)) != NULL) {
          if ((strncmp(direntry->d_name, ".", 1) != 0)) {
            char src_subdir[300];
            strcpy(src_subdir, src);
            strcat(src_subdir, "/");
            strcat(src_subdir, direntry->d_name);

            char dest_subdir[300];
            strcpy(dest_subdir, dest);
            strcat(dest_subdir, "/");
            strcat(dest_subdir, direntry->d_name);

            if (lstat(src_subdir, &status) == -1) {  //Deal with unsuccessful lstat
              perror("lstat");
              return -num_of_processes;
            }
            if (S_ISDIR(status.st_mode)) {   //subdirectory file is a directory
              pid_t pid = fork();
              if (pid == 0) {
                mkdir(dest_subdir, status.st_mode);
                copy_ftree(src_subdir, dest_subdir);
              } else {
                pid_array[num_of_pid++] = pid;
                num_of_processes++;
              }
            } else if (S_ISREG(status.st_mode)) {
              copy_file(src_subdir, dest_subdir);
            }
          }
        }
        int wait_status = 0;
        for (int i = 0; i < num_of_pid; i++) {
          waitpid(pid_array[i], &wait_status, 0);
        }
      }
      closedir(dir);
    }
    if (lstat(src, &status) == -1) {  //Deal with unsuccessful lstat
      perror("lstat");
      return -num_of_processes;
    }
    else if ((S_ISREG(status.st_mode)) && ((strncmp(src, ".", 1) != 0))) {   //source is a regular file
        char dest_file2[300];
        char *pos = strrchr(src, '/');
        if (pos == NULL){
          copy_file(src, dest);
        } else {
          strcpy(dest_file2, dest);
          strcat(dest_file2, "/");
          strcat(dest_file2, pos);
          copy_file(src, dest_file2);
        }
        num_of_processes = 1;
    }

  return num_of_processes;
}
